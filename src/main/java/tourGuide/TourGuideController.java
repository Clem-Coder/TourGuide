package tourGuide;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jsoniter.output.JsonStream;

import gpsUtil.location.VisitedLocation;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;
import tripPricer.Provider;


/**
 * here are all the endpoints for TourGuide
 */

@RestController
public class TourGuideController {

	@Autowired
	TourGuideService tourGuideService;
	
    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    private static final Logger logger = LogManager.getLogger("TourGuideController");



    /**
     * Give the location of a User
     *
     * @param userName the name of the user, for example 'internalUser1'
     * @return a visitedLocation object that contains the location of the user (longitude, latitude)
     */

    @RequestMapping("/getLocation") 
    public String getLocation(@RequestParam String userName) {
        logger.info("New request: get user location");
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
		return JsonStream.serialize(visitedLocation.location);
    }
    

    /**
     * Give the 5 closest attractions to the user
     *
     * @param userName the name of the user, for example 'internalUser1'
     * @return a UserNearbyAttraction list  that contains the 5 closest attractions to the user, and the reward points for visited each attraction
     */

    @RequestMapping("/getNearbyAttractions") 
    public String getNearbyAttractions(@RequestParam String userName) {
        logger.info("New request: get user nearby attractions");
        User user = tourGuideService.getUser(userName);
    	return JsonStream.serialize(tourGuideService.getUserNearbyAttractions(user));
    }


    /**
     * Give the reward points of the user
     *
     * @param userName the name of the user, for example 'internalUser1'
     * @return the reward points of the user
     */
    
    @RequestMapping("/getRewards") 
    public String getRewards(@RequestParam String userName) {
        logger.info("New request: get user rewards");
        return JsonStream.serialize(tourGuideService.getUserRewards(getUser(userName)));
    }

    //test
    /**
     * Give the UUID and the location (longitude, latitude) of all the users
     *
     * @return a list that contains the UUID and the location (longitude, latitude) of all the users
     */

    @RequestMapping("/getAllCurrentLocations")
    public String getAllCurrentLocations() {

        logger.info("New request: get all user current locations");
        List<VisitedLocation> visitedLocations = new ArrayList<>();
        List<User> users = tourGuideService.getAllUsers();
        List<String> allCurrentLocations = new ArrayList<>();

        users.forEach(user -> visitedLocations.add(tourGuideService.getUserLocation(user)));
        visitedLocations.forEach(visitedLocation -> allCurrentLocations.add(visitedLocation.userId.toString()
                                                                                     + " : {longitude: " + visitedLocation.location.longitude
                                                                                     + ",  latitude: " + visitedLocation.location.latitude + "}"));

    	return JsonStream.serialize(allCurrentLocations);
    }

    /**
     * Give a list of trips deals for the user with his preferences
     *
     * @param userName the name of the user, for example 'internalUser1'
     * @return a list of 5 providers with there price, selected with the user preferences
     */

    @RequestMapping("/getTripDeals")
    public String getTripDeals(@RequestParam String userName) {
        logger.info("New request: get user trip deals");
        List<Provider> providers = tourGuideService.getTripDeals(getUser(userName));
    	return JsonStream.serialize(providers);
    }

    /**
     * Give all the user information
     *
     * @param userName the name of the user, for example 'internalUser1'
     * @return a User object that contain all the information about the user (UUDI, userName, phoneNumber etc..)
     */
    
    private User getUser(String userName) {
        logger.info("New request: get user by user name");
        return tourGuideService.getUser(userName);
    }

    //test
   

}