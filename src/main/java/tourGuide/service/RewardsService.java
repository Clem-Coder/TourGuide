package tourGuide.service;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.user.User;
import tourGuide.user.UserReward;


/**
 * here are all the methods for calculate user rewards
 */
@Service
public class RewardsService {

    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

    // proximity in miles
    private       int           defaultProximityBuffer   = 10;
    private       int           proximityBuffer          = defaultProximityBuffer;
    private       int           attractionProximityRange = 200;
    private final GpsUtil       gpsUtil;
    private final RewardCentral rewardsCentral;

    private static final Logger logger = LogManager.getLogger("RewardsService");

    public RewardsService(GpsUtil gpsUtil, RewardCentral rewardCentral) {
        logger.info("create new RewardsService");
        this.gpsUtil        = gpsUtil;
        this.rewardsCentral = rewardCentral;
    }




    /**
     *  allows to define the distance between a user and an attraction
     *
     * @param proximityBuffer the distance between a user and an attraction
     *
     */

    public void setProximityBuffer(int proximityBuffer) {
        logger.info("New request: setting proximityBuffer");
        this.proximityBuffer = proximityBuffer;
    }



    public void setDefaultProximityBuffer() {

        proximityBuffer = defaultProximityBuffer;
    }




    /**
     * Calculate the reward points for a user (with one thread)
     *
     * @param user the user whose reward points are to be calculated
     *
     */
    public void calculateRewards(User user) {

        logger.info("New request: calculate rewards for user: " + user.getUserId().toString());
        List<VisitedLocation> userLocations = new CopyOnWriteArrayList<>(user.getVisitedLocations());
        List<Attraction>      attractions   = gpsUtil.getAttractions();

        for (VisitedLocation visitedLocation : userLocations) {
            for (Attraction attraction : attractions) {
                if (user.getUserRewards()
                        .stream()
                        .filter(r -> r.attraction.attractionName.equals(attraction.attractionName))
                        .count() == 0) {
                    if (nearAttraction(visitedLocation, attraction)) {
                        user.addUserReward(new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user)));
                    }
                }
            }
        }
    }



    /**
     * Calculate the reward points for a  list of users (with multithreading)
     *
     * @param users a list of users
     *
     */

    public void calculateRewardWithMultiThreading(List<User> users) {

        logger.info("New request: calculate rewards for a list of " + users.size() +" users");

        ExecutorService executorService = Executors.newFixedThreadPool(100);

        users.forEach(user -> {
            Runnable runnable = () -> {calculateRewards(user);};
            executorService.submit(runnable);
        });

        executorService.shutdown();

        try {
            executorService.awaitTermination(20, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    /**
     * Watch if the user location is near the attraction
     *
     * @param attraction the attraction location
     * @param location the user location
     *
     * @return true if the user is near the attraction, false otherwise
     */

        public boolean isWithinAttractionProximity (Attraction attraction, Location location){
            logger.info("New request: check if the user is near the attraction " + attraction.attractionName);
            return getDistance(attraction, location) > attractionProximityRange ? false : true;
        }




    /**
     * checks if the attraction the user just visited is close to another attraction
     *
     * @param attraction the attraction location
     * @param visitedLocation attraction the user just visited
     *
     * @return true if the attraction the user just visited is near the attraction, false otherwise
     */

        private boolean nearAttraction (VisitedLocation visitedLocation, Attraction attraction){
            logger.info("New request: check if the user is near the attraction " + attraction.attractionName);
            return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
        }




    /**
     * Give the reward points if the user chose the visited an attraction
     *
     * @param attraction the attraction location
     * @param user the user whose reward points are to be calculated
     *
     * @return the reward points if the user chose the visited an attraction
     */
        public int getRewardPoints (Attraction attraction, User user){
            logger.info("New request: get the reward points for visiting " + attraction.attractionName);
            return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
        }


    /**
     * Give the distance between two locations (longitude, latitude)
     *
     * @param loc1 the first location
     * @param loc2 the second location
     *
     * @return the distance between the first and the second location
     */

        public double getDistance (Location loc1, Location loc2){
            logger.info("New request: get between two locations");
            double lat1 = Math.toRadians(loc1.latitude);
            double lon1 = Math.toRadians(loc1.longitude);
            double lat2 = Math.toRadians(loc2.latitude);
            double lon2 = Math.toRadians(loc2.longitude);

            double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

            double nauticalMiles = 60 * Math.toDegrees(angle);
            double statuteMiles  = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
            return statuteMiles;
        }

    }
