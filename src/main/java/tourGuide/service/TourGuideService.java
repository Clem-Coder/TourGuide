package tourGuide.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.helper.InternalTestHelper;
import tourGuide.tracker.Tracker;
import tourGuide.user.User;
import tourGuide.user.UserNearbyAttraction;
import tourGuide.user.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;


/**
 * here are all the methods to get or add user information
 */
@Service
public class TourGuideService {

    private       Logger         logger     = LoggerFactory.getLogger(TourGuideService.class);
    private final GpsUtil        gpsUtil;
    private final RewardsService rewardsService;
    private final TripPricer     tripPricer = new TripPricer();
    public final  Tracker        tracker;
    boolean testMode = true;


    public TourGuideService(GpsUtil gpsUtil, RewardsService rewardsService) {
        logger.info("New TourGuideService");
        this.gpsUtil        = gpsUtil;
        this.rewardsService = rewardsService;

        if (testMode) {
            logger.info("TestMode enabled");
            logger.debug("Initializing users");
            initializeInternalUsers();
            logger.debug("Finished initializing users");
        }
        tracker = new Tracker(this);
        addShutDownHook();
    }


    /**
     *  allows to get user rewards
     *
     * @param user the user whose rewards are to be obtained
     *
     * @return a list that contains the user's rewards
     */
    public List<UserReward> getUserRewards(User user) {
        logger.info("use getUserRewards method of TourGuideService");

        return user.getUserRewards();
    }



    /**
     *  allows to get user visited location
     *
     * @param user the user whose visited location is to be obtained
     *
     * @return the user last visited location if he has already visited a location, otherwise track the user location
     */

    public VisitedLocation getUserLocation(User user) {
        logger.info("use getUserLocation method of TourGuideService with user " + user.getUserId().toString());

        VisitedLocation visitedLocation = (user.getVisitedLocations()
                                               .size() > 0) ? user.getLastVisitedLocation() : trackUserLocation(user);
        return visitedLocation;
    }

    /**
     *  allows to get user's information
     *
     * @param userName the name of the user
     *
     * @return a user object that contain all the user information
     */
    public User getUser(String userName) {
        logger.info("use getUser method of TourGuideService with userName " + userName);

        return internalUserMap.get(userName);
    }


    /**
     *  allows to get all users

     * @return a list that contains all users
     */

    public List<User> getAllUsers() {
        logger.info("use getAllUsers method of TourGuideService");

        return internalUserMap.values().stream().collect(Collectors.toList());
    }


    /**
     *  allows to add a new user
     *
     * @param user the user to add
     *
     */
    public void addUser(User user) {
        logger.info("use addUser method of TourGuideService");

        if (!internalUserMap.containsKey(user.getUserName())) {
            internalUserMap.put(user.getUserName(), user);
        }
    }



    /**
     *  allows to get a list of providers for a user
     *
     * @param user the user whose providers are to be obtained
     *
     * @return a list of providers obtained from user preferences
     */
    public List<Provider> getTripDeals(User user) {
        logger.info("use getTripDeals method of TourGuideService with user " + user.getUserId().toString());

        int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
        List<Provider> providers = tripPricer.getPrice(tripPricerApiKey,user.getUserId(),
                                                                        user.getUserPreferences().getNumberOfAdults(),
                                                                        user.getUserPreferences().getNumberOfChildren(),
                                                                        user.getUserPreferences().getTripDuration(),
                                                                        cumulatativeRewardPoints);
        user.setTripDeals(providers);
        return providers;
    }




    /**
     *  allows you to obtain a list that contains the 5 attractions closest to the user
     *
     * @param user the user whose 5 closest attractions are to be obtained
     *
     * @return a list that contains the 5 attractions closest to the user
     */
    public List<UserNearbyAttraction> getUserNearbyAttractions(User user) {
        logger.info("use getUserNearbyAttractions method of TourGuideService with user " + user.getUserId().toString());

        List<UserNearbyAttraction> userNearbyAttractions = new ArrayList<>();
        List<Attraction>           attractions           = gpsUtil.getAttractions();
        VisitedLocation            visitedLocation       = getUserLocation(user);

        for (Attraction attraction : attractions) {
            userNearbyAttractions.add(new UserNearbyAttraction(attraction.attractionName, attraction.latitude, attraction.longitude, visitedLocation.location.latitude, visitedLocation.location.longitude, rewardsService.getDistance(attraction, visitedLocation.location), rewardsService.getRewardPoints(attraction, user)));
        }
        userNearbyAttractions = userNearbyAttractions.stream()
                                                     .sorted(Comparator.comparingDouble(UserNearbyAttraction::getDistanceBetweenUserAndAttraction))
                                                     .limit(5)
                                                     .collect(Collectors.toList());

        return userNearbyAttractions;
    }




    /**
     *  allows to track a user and get his location
     *
     * @param user the user whose visited location is to be obtained
     *
     * @return the user location
     */
    public VisitedLocation trackUserLocation(User user) {
        logger.info("use trackUserLocation method of TourGuideService with user " + user.getUserId().toString());

        VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
        user.addToVisitedLocations(visitedLocation);
        rewardsService.calculateRewards(user);
        return visitedLocation;
    }



    /**
     *  allows to track a list of users and get there location with multithreading
     *  if all users are not tracked in less than 15 minutes, the method fails
     *
     * @param users the user's list
     *
     */
    public void trackUsersWithMultiThreading(List<User> users) {
        logger.info("use trackUsersWithMultiThreading method with a users list of: " + users.size());

        ExecutorService executorService = Executors.newFixedThreadPool(100);

        users.stream().forEach(user -> {
            Runnable runnable = () -> {trackUserLocation(user);};
            executorService.submit(runnable);
        });

        executorService.shutdown();

        try {
            executorService.awaitTermination(15, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    private void addShutDownHook() {

        Runtime.getRuntime().addShutdownHook(new Thread() {

            public void run() {

                tracker.stopTracking();
            }
        });
    }

    /**********************************************************************************
     *
     * Methods Below: For Internal Testing
     *
     **********************************************************************************/
    private static final String            tripPricerApiKey = "test-server-api-key";
    // Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
    private final        Map<String, User> internalUserMap  = new HashMap<>();




    /**
     *  Initialize all users with the internal User Number
     */

    private void initializeInternalUsers() {

        IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
            String userName = "internalUser" + i;
            String phone    = "000";
            String email    = userName + "@tourGuide.com";
            User   user     = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);

            internalUserMap.put(userName, user);
        });
        logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
    }

    private void generateUserLocationHistory(User user) {

        IntStream.range(0, 3).forEach(i -> {
            user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
        });
    }

    private double generateRandomLongitude() {

        double leftLimit  = -180;
        double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private double generateRandomLatitude() {

        double leftLimit  = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private Date getRandomTime() {

        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }

}
